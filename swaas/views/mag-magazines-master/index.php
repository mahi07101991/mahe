<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MagMagazinesMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mag Magazines Masters';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mag-magazines-master-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mag Magazines Master', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'Magazine_Name',
            'Magazine_Product',
            'Magazine_Thumbnail',
            'Updated_On',
            // 'Is_Deleted:boolean',
            // 'Author_User_Id',
            // 'Magazine_Description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
