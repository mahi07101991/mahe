<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MagMagazinesMaster */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mag Magazines Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mag-magazines-master-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'Magazine_Name',
            'Magazine_Product',
            'Magazine_Thumbnail',
            'Updated_On',
            'Is_Deleted:boolean',
            'Author_User_Id',
            'Magazine_Description',
        ],
    ]) ?>

</div>
