<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MagMagazinesMaster */

$this->title = 'Create Mag Magazines Master';
$this->params['breadcrumbs'][] = ['label' => 'Mag Magazines Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mag-magazines-master-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
