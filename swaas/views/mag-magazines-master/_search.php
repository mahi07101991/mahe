<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MagMagazinesMasterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mag-magazines-master-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'Magazine_Name') ?>

    <?= $form->field($model, 'Magazine_Product') ?>

    <?= $form->field($model, 'Magazine_Thumbnail') ?>

    <?= $form->field($model, 'Updated_On') ?>

    <?php // echo $form->field($model, 'Is_Deleted')->checkbox() ?>

    <?php // echo $form->field($model, 'Author_User_Id') ?>

    <?php // echo $form->field($model, 'Magazine_Description') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
