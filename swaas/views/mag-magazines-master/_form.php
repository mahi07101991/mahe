<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MagMagazinesMaster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mag-magazines-master-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Magazine_Name')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'Magazine_Product')->textInput() ?>

    <?= $form->field($model, 'Magazine_Thumbnail')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'Updated_On')->textInput() ?>

    <?= $form->field($model, 'Is_Deleted')->checkbox() ?>

    <?= $form->field($model, 'Author_User_Id')->textInput() ?>

    <?= $form->field($model, 'Magazine_Description')->textInput(['maxlength' => 45]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
