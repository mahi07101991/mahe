<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdmEmailTemplates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adm-email-templates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mail_template')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'mail_subject')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'mail_body')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
