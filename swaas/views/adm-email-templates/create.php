<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AdmEmailTemplates */

$this->title = 'Create Adm Email Templates';
$this->params['breadcrumbs'][] = ['label' => 'Adm Email Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adm-email-templates-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
