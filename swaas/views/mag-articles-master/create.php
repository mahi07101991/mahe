<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MagArticlesMaster */

$this->title = 'Create Mag Articles Master';
$this->params['breadcrumbs'][] = ['label' => 'Mag Articles Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mag-articles-master-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
