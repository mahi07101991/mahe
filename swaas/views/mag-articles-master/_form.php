<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MagArticlesMaster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mag-articles-master-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'Article_Name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Unio_Book_Id')->textInput() ?>

    <?= $form->field($model, 'Article_State')->textInput() ?>

    <?= $form->field($model, 'Published_Date')->textInput() ?>

    <?= $form->field($model, 'Expiry_Date')->textInput() ?>

    <?= $form->field($model, 'Author_User_Id')->textInput() ?>

    <?= $form->field($model, 'Author_Unio_User_Id')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'Creation_Date')->textInput() ?>

    <?= $form->field($model, 'Article_Thumbnail')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'Updated_On')->textInput() ?>

    <?= $form->field($model, 'Is_Deleted')->checkbox() ?>

    <?= $form->field($model, 'Article_Description')->textInput() ?>

    <?= $form->field($model, 'Magazine_Id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
