<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MagArticlesMaster */

$this->title = 'Update Mag Articles Master: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mag Articles Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mag-articles-master-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
