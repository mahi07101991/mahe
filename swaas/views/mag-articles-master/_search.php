<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MagArticlesMasterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mag-articles-master-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'Article_Name') ?>

    <?= $form->field($model, 'Unio_Book_Id') ?>

    <?= $form->field($model, 'Article_State') ?>

    <?= $form->field($model, 'Published_Date') ?>

    <?php // echo $form->field($model, 'Expiry_Date') ?>

    <?php // echo $form->field($model, 'Author_User_Id') ?>

    <?php // echo $form->field($model, 'Author_Unio_User_Id') ?>

    <?php // echo $form->field($model, 'Creation_Date') ?>

    <?php // echo $form->field($model, 'Article_Thumbnail') ?>

    <?php // echo $form->field($model, 'Updated_On') ?>

    <?php // echo $form->field($model, 'Is_Deleted')->checkbox() ?>

    <?php // echo $form->field($model, 'Article_Description') ?>

    <?php // echo $form->field($model, 'Magazine_Id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
