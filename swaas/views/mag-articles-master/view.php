<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MagArticlesMaster */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mag Articles Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mag-articles-master-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'Article_Name',
            'Unio_Book_Id',
            'Article_State',
            'Published_Date',
            'Expiry_Date',
            'Author_User_Id',
            'Author_Unio_User_Id',
            'Creation_Date',
            'Article_Thumbnail',
            'Updated_On',
            'Is_Deleted:boolean',
            'Article_Description',
            'Magazine_Id',
        ],
    ]) ?>

</div>
