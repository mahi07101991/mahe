<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MagArticlesMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mag Articles Masters';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mag-articles-master-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mag Articles Master', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'Article_Name',
            'Unio_Book_Id',
            'Article_State',
            'Published_Date',
            // 'Expiry_Date',
            // 'Author_User_Id',
            // 'Author_Unio_User_Id',
            // 'Creation_Date',
            // 'Article_Thumbnail',
            // 'Updated_On',
            // 'Is_Deleted:boolean',
            // 'Article_Description',
            // 'Magazine_Id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
