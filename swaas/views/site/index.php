<?php

$this->title = 'Rep/Admin';
/*$this->registerJsFile('js/jquery.js');*/
$this->registerJsFile('js/rep.js');
$this->registerJsFile('js/admin.js');
$this->registercssFile('css/admin1.css');
$this->registerJsFile('tinymce/tinymce.min.js');
$this->registerJsFile('tinymce/script.js');
?>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</head>
<body>
<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading" id="panel-p1">
            <h4 class="panel-title" id="article-heading">
                <a data-toggle="collapse" id="article-id" data-parent="#accordion" href="#collapse1">
                    ARTICLE</a>
                <span class="noti_Container article-menu-list glyphicon glyphicon-list-alt"><span class="noti_bubble">55</span></span>
                <span class="noti_Container article-menu-list glyphicon glyphicon-envelope"><span class="noti_bubble">25</span></span>
                <span class="noti_Container article-menu-list glyphicon glyphicon-bell"><span class="noti_bubble">17</span></span>
                <span class="noti_Container article-menu-list glyphicon glyphicon-flash"><span class="noti_bubble">14</span></span>
            </h4>

        </div>
        <div id="collapse1" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4 col-sm-4 col-md-4  field1" id="article">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default " data-toggle="modal" id="create-article">
                                Create
                            </button>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default ">Edit</button>
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu" class="edit">
                                <li><a href="#" id="update-article" style="color:orange;">Update</a></li>
                                <li><a href="#" id="delete-article" style="color:orange;">Delete</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 field" id="displayAllMagazines">

                    </div>
                    <div class="col-lg-4 col-md-4 field">
                        <h2>Field</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--   Magazine    -->
    <div class="panel panel-default">
        <div class="panel-heading" id="panel-p2">
            <h4 class="panel-title" id="magazine-heading">
                <a data-toggle="collapse" id="magazine-id" data-parent="#accordion" href="#collapse2" >
                    MAGAZINE</a>
            </h4>
        </div>
        <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4 col-sm-4 col-md-4 field1" >
                        <div class="btn-group">
                            <button type="button" class="btn btn-default" data-toggle="modal" id="create-magazine">
                                Create
                            </button>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default ">Edit</button>
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu" class="edit">
                                <li><a href="#" id="update-magazine" style="color:orange;">Update</a></li>
                                <li><a href="#" id="delete-magazine" style="color:orange;">Delete</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 field" id="displayAllArticles" >
                    </div>
                    <div class="col-lg-4 col-md-4 field">
                        <button type="button" class="btn btn-default edit"></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" id="panel-p3">
            <h4 class="panel-title" id="session-heading">
                <a data-toggle="collapse" id="session-id" data-parent="#accordion" href="#collapse3">
                    SESSION</a>
            </h4>
        </div>
        <div id="collapse3" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4 col-sm-4 col-md-4 field1" >
                        <div class="btn-group">
                            <button type="button" class="btn btn-default" data-toggle="modal" id="create-session">
                                Create
                            </button>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default ">Edit</button>
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu" class="edit">
                                <li><a href="#" id="update-session" style="color:orange;">Update</a></li>
                                <li><a href="#" id="delete-session" style="color:orange;">Delete</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 field">
                        <h2>Field</h2>
                    </div>
                    <div class="col-lg-4 col-md-4 field">
                        <h2>Field</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

        <!--MODEL CREATE Article-->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog" style="border:0;padding:0;">
        <div class="modal-content">
            <div class="modal-header"><!--header starting-->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create Article</h4>
            </div><!--heading-->
            <form role="form" method="post" class="form-horizontal"><!--form starting-->
                <!--<input type="hidden" name="_csrf" value="<?/*= Yii::$app->request->getCsrfToken(); */?>"/>-->
                    <div class="modal-body"><!--bodying starting-->
                        <div class="row"><!--Brand starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Magazines<span style="color:red">*</span></label>
                                <div class="col-lg-6">
                                    <select class="form-control" id="getingMagazine">
                                    </select>
                                </div>
                            </div>
                        </div><!--Brand ending-->
                        <div class="row"><!--article starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Article Name<span style="color:red">*</span></label>
                                <div class="col-lg-6" id="">
                                    <input type="text" class="form-control" id="articleName" placeholder="Please enter Article name ">
                                </div>
                            </div>
                        </div><!--article row ending-->

                        <div class="row"><!--description starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Description<span style="color:red">*</span></label>
                                <div class="col-lg-6" class="form-control" >
                                    <textarea class="form-control" id="articleDesc" rows="3" cols="2" placeholder="Please ente Description"></textarea>
                                </div>
                            </div>
                        </div><!--Description ending-->

                        <div class="row"><!--Image browse starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Thumbnail<span style="color:red">*</span></label>
                                <div class="col-lg-6" id="selected-thumbnail">
                                    <input id="article-thumbnail" name="MagMagazinesMaster[Magazine_Thumbnail]" class="form-control" type="file">
                                    <div class="row">
                                        <div class="col-lg-4 col-lg-offset-4" id="article-image-preview"></div><!--Image previewing here-->
                                    </div>
                                </div>
                            </div>
                        </div><!--image browse ending-->
                    </div><!--bodying ending-->
                    <div class="modal-footer"><!--footer starting-->
                        <button type="submit" class="btn btn-default" onclick="setarticleToMagazine()" data-dismiss="modal" style="background-color:#a7dda8;color:white;">Create</button>
                    </div><!--footer ending-->
            </form><!--form ending-->
        </div>
    </div>
</div>

<!--create Magazine Modal-->
<div class="modal fade" id="createMModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog" style="border:0px;padding:0px;">
        <div class="modal-content">
            <div class="modal-header"><!--header starting-->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create Magazine</h4>
            </div><!--heading-->
            <form role="form" id="magazine-form" method="post" class="form-horizontal"><!--form starting-->

                <div class="modal-body"><!--body starting-->
                        <div class="row"><!--magazine starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Magazine Name<span style="color:red">*</span></label>
                                <div class="col-lg-6" id="">
                                    <input type="text" id="Magazine_Name" class="form-control" name="MagMagazinesMaster[Magazine_Name]" placeholder="Please enter Magazine name ">
                                </div>
                            </div>
                        </div><!--magazine row ending-->
                        <div class="row"><!--Product starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Product<span style="color:red">*</span></label>
                                <div class="col-lg-6" class="form-control" id="products">
                                    <select class="form-control" name="MagMagazinesMaster[Magazine_Product]" id="select-brand"></select>
                                </div>
                            </div>
                        </div><!--Product ending-->

                    <div class="row"><!--description starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Description<span style="color:red">*</span></label>
                                <div class="col-lg-6" class="form-control" >
                                    <textarea class="form-control" id="Magazine_Description" name="MagMagazinesMaster[Magazine_Description]" rows="3" cols="2" placeholder="Please enter Description"></textarea>
                                </div>
                            </div>
                        </div><!--Description ending-->

                        <div class="row"><!--Image browse starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Thumbnail<span style="color:red">*</span></label>
                                <div class="col-lg-6" id="selected-thumbnail">
                                    <input id="magazine-thumbnail" name="MagMagazinesMaster[Magazine_Thumbnail]" class="form-control" type="file">
                                    <div class="row">
                                    <div class="col-lg-4 col-lg-offset-4" id="magazine-image-preview"></div><!--Image previewing here-->
                                    </div>
                                </div>
                            </div>
                        </div><!--image browse ending-->
            </div><!--bodying ending-->
            <div class="modal-footer"><!--footer starting-->
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="sendMagazineToController()" style="background-color:#a7dda8;color:white;">Create</button>
            </div><!--footer ending-->
            </form><!--form ending-->
        </div>
    </div>
</div>


<!--create Session Modal-->
<div class="modal fade" id="createSModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog" style="border:0px;padding:0px;">
        <div class="modal-content">
            <div class="modal-header"><!--header starting-->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create Session</h4>
            </div><!--heading-->
            <div class="modal-body"><!--bodying starting-->
                <div>
                    <form role="form" class="form-horizontal"><!--form starting-->

                        <div class="row"><!--article starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Session Name<span style="color:red">*</span></label>
                                <div class="col-lg-6" id="">
                                    <input type="text" class="form-control" placeholder="Please enter Session name ">
                                </div>
                            </div>
                        </div><!--article row ending-->

                        <div class="row"><!--description starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Description<span style="color:red">*</span></label>

                                <div class="col-lg-6" class="form-control">
                                    <textarea class="form-control" rows="3" cols="2" placeholder="Please ente Description"></textarea>
                                </div>
                            </div>
                        </div><!--Description ending-->

                        <div class="row"><!--Image browse starting-->
                            <div class="form-group">

                                <label class="col-lg-4 col-lg-offset-1">Thumbnail<span style="color:red">*</span></label>
                                <div class="col-lg-6">
                                    <input class="form-control" type="file">
                                </div>
                            </div>
                        </div><!--image browse ending-->
                    </form><!--form ending-->
                </div>
            </div><!--bodying ending-->
            <div class="modal-footer"><!--footer starting-->
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn" style="background-color:#a7dda8;color:white;">Create</button>
            </div><!--footer ending-->
        </div>
    </div>
</div>
                <!--Delete Article modal-->
<div class="modal fade bs-example-modal-sm" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Delete Article</h4>
            </div>
            <form role="form" class="form-horizontal"><!--form starting-->
            <div class="modal-body">
                    <div class="row"><!--article delete starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Select Article<span style="color:red">*</span></label>
                                <div class="col-lg-6" id="">
                                    <select class="form-control" id="delete-list-for-articles">
                                    </select>
                                </div>
                            </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onclick="deleteArticle()"
                        style="background- color:#a7dda8;color:white;">Delete
                </button>
            </div>
            </form>
            <!--form ending-->
        </div>
    </div>
</div>

<!--Delete Article modal-->
<div class="modal fade bs-example-modal-sm" id="deleteMModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Delete Magazine</h4>
            </div>
            <div class="modal-body">
                <form role="form" class="form-horizontal"><!--form starting-->
                    <div class="row"><!--article delete starting-->
                        <div class="form-group">
                            <label class="col-lg-4 col-lg-offset-1">Select Magazine<span style="color:red">*</span></label>
                            <div class="col-lg-6" id="">
                                <select class="form-control">
                                    <option value="Magazine 1">Magazine 1</option>
                                    <option value="Magazine 2">Magazine 2</option>
                                    <option value="Magazine 3">Magazine 3</option>
                                    <option value="Magazine 4">Magazine 4</option>
                                    <option value="Magazine 5">Magazine 5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form><!--form ending-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn " style="background-color:#a7dda8;color:white;">Delete</button>
            </div>
        </div>
    </div>
</div>

<!--Delete Article modal-->
<div class="modal fade bs-example-modal-sm" id="deleteSModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Delete Session</h4>
            </div>
            <div class="modal-body">
                <form role="form" class="form-horizontal"><!--form starting-->
                    <div class="row"><!--article delete starting-->
                        <div class="form-group">
                            <label class="col-lg-4 col-lg-offset-1">Select Session<span style="color:red">*</span></label>
                            <div class="col-lg-6" id="">
                                <select class="form-control">
                                    <option value="Session 1">Session 1</option>
                                    <option value="Session 2">Session 2</option>
                                    <option value="Session 3">Session 3</option>
                                    <option value="Session 4">Session 4</option>
                                    <option value="Session 5">Session 5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form><!--form ending-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn " style="background-color:#a7dda8;color:white;">Delete</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Article</h4>
            </div>
            <form role="form" class="form-horizontal"><!--form starting-->
            <div class="modal-body">

                    <div class="row"><!--article delete starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Select Article<span style="color:red">*</span></label>
                                <div class="col-lg-6" id="">
                                    <select class="form-control" id="update-list-for-articles">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--hiding article-->
                        <div class="row"><!--article starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Article Name<span style="color:red">*</span></label>
                                <div class="col-lg-6" >
                                    <input type="text" class="form-control" placeholder="Please enter Article name ">
                                </div>
                            </div>
                        </div><!--article row ending-->

                        <div class="row"><!--description starting-->
                            <div class="form-group">
                                <label class="col-lg-4 col-lg-offset-1">Description<span style="color:red">*</span></label>
                                <div class="col-lg-6" class="form-control" >
                                    <textarea class="form-control" rows="3" cols="2" placeholder="Please ente Description"></textarea>
                                </div>
                            </div>
                        </div><!--Description ending-->



                        <div class="row"><!--Image browse starting-->
                            <div class="form-group">

                                <label class="col-lg-4 col-lg-offset-1">Thumbnail<span style="color:red">*</span></label>
                                <div class="col-lg-6">
                                    <input class="form-control" type="file">
                                </div>
                            </div>
                        </div><!--image brow
                    </div><!--row ending-->

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="deleteArticle()"
                        style="background- color:#a7dda8;color:white;">Delete
                </button>

            </div>
            </form><!--form ending-->
        </div>
    </div>
</div>

<!--Email Template Modal-->
<div class="modal fade bs-example-modal-lg" id="CreateEmailTemplateModel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Create Email Template</h4>
            </div>
            <form class="form-horizontal" role="form" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="template" class="col-lg-2">Template</label>
                        <div class="col-lg-10" class="form-control" id="templates">
                            <select class="form-control" type="text" onchange="gettemplatedetails()"id="select-template"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="subject" class="col-lg-2">Subject</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name = "AdmEmailTemplates[mail_subject]" placeholder="Subject" id="template-subject">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="body" class="col-lg-2">Body</label>
                        <div class="col-lg-10" style="padding:5px;" >
                            <textarea id="textarea1" style="width:100%" rows="16"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button  type="button" data-dismiss="modal" class="btn btn-default" onclick="SendTemplateToController()" style="margin-top:15px;" id="save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" id="publishArticle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Publish Article</h4>
            </div>
            <form role="form" class="form-horizontal"><!--form starting-->
                <div class="modal-body">

                    <div class="row"><!-- select Email Template starting-->
                        <div class="form-group">
                            <label class="col-lg-4 col-lg-offset-1">Email Template<span style="color:red">*</span></label>
                            <div class="col-lg-6" >
                                <div class="col-lg-6" id="">
                                    <select class="form-control" id="email-template-list">

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div><!--article row ending-->

                    <div class="row"><!--doctors starting-->
                        <div class="form-group">
                            <label class="col-lg-4 col-lg-offset-1">Doctor<span style="color:red">*</span></label>
                            <div class="col-lg-6" id="">
                                <select class="form-control" id="doctors-list">

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"  style="background- color:#a7dda8;color:white;">
                        <span class="glyphicon glyphicon-send" class="button-box" aria-hidden="true">Send</span>
                    </button>

                </div>
            </form><!--form ending-->
        </div>
    </div>
</div>

<input type="hidden" value="" id="hidden-field">
</body>