<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use yii\grid\GridView;
use yii\helpers\Url;
use yii\web\Request;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->registerCssFile('logincss/logincss.css',['media'=>'print']);
$this->registerJsFile('loginjs/loginjs.js');
/*Use this pattern for email check*/
/*"/^([\w-\.+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/"*/
?>
<head>
    <meta charset="UTF-8">
     <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <style type="text/css">
    img {
      vertical-align: middle;
        border: 0;
        margin-left:95px;
    }

    #proceed{
        margin:0px auto;
        margin-top:25px;
        margin-left:-3px;
    }
    /*.form-control {
  display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
  -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
  transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}*/
    #header{
          background: -webkit-linear-gradient(right,#181149,#26a9e0); /* For Safari 5.1 to 6.0 */
          background: -o-linear-gradient(right,#181149,#26a9e0); /* For Opera 11.1 to 12.0 */
          background: -moz-linear-gradient(right,#181149,#26a9e0); /* For Firefox 3.6 to 15 */
          background: linear-gradient(to right,#181149,#26a9e0); /* Standard syntax */
    }

    </style>
</head>

<body>
<div style="height:460px;width:485px;margin:0px auto;margin-top: 60px;/*border:1px solid black;*/">
    <div class="well" style="border-radius:0px;width:455px;height:160px;/*border:1px  solid red;*/">
        <?= Html::img('images/logo.png',['id'=>'logo','style'=>'width: 200px;margin-top: 36px;']);?>
    </div>
    <div style="width:455px;border-radius:0px;height:290px;margin-top:5px;/*border:1px solid green;*/"class="well" >
        <form role="form" action="index.php?r=site/login" method="post">
            <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken(); ?>"/>
            <div class="row">
                <div style=" margin-top:8px;"class="col-xs-8 col-sm-8 col-md-8 col-xs-offset-2 col-sm-offset-2  col-md-offset-2">
                    <div class="form-group " id="master-email-login">
                        <?php 
                        $temp="";
                            if(isset($setpassword))
                                $temp=$setpassword['User_Email'];
                        ?>
                        <label><?=isset($setinvalid)?"Invalid Details":"";?></label>
                        <input type="email"style="border-radius: 0px;border-top-color: black;margin-top: 20px;" name="LoginForm[username]" class="form-control" id="ema-login" placeholder="Email" value=<?=$temp;?> >
                        <?php 
                        if(isset($setpassword)){
                        if($setpassword['User_Type']==1 || $setpassword['User_Type']==2 || $setpassword['User_Type']==3 )
                           {
                            echo '<input type="password"style="border-radius: 0px;border-top-color: black;margin-top: 20px;" name="LoginForm[password]" class="form-control" id="password" placeholder="Password">';
                        }
                        }
                        ?>
                        
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 col-md-2 col-lg-2 text-center ">
                    <button style="border:0px;background-color:transparent;" id ="email_button" type="submit" class="btn btn-default"><?= Html::img('images/proceed.png',['id'=>'proceed','style'=>'height:35px;']);?></button>
                </div>
            </div>
        </form>
        <div style="text-align: center">
            <a href="#" data-toggle="modal" data-target="#myModal">New HCP Registration</a>
        </div>
    </div>

</div>

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" id="header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="color:white;">New HCP Registration</h4>
        </div>
        <div class="modal-body">
          <form role="form" method="post" action="/editingswaas/web/index.php?r=usr-doctors-master/hcpcreate"  class="form-horizontal ">
          <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken(); ?>"/>
                        <div class="form-group" >
                            <label class="col-sm-3 col-md-3 col-lg-3 control-label">First Name <span class="mandatoryField">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name = "UsrUsersMaster[User_First_Name]" class="form-control "  required="" placeholder="First name of the HCP" >
                                <span class="errorIcon "  tooltip="" tooltip-trigger="click" tooltip-placement="right" tooltip-append-to-body="true">!</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-lg-3 control-label">Last Name<span class="mandatoryField">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name = "UsrUsersMaster[User_Last_Name]" class="form-control "placeholder="Last name of the HCP" required="">
                                <span class="errorIcon " tooltip="" tooltip-trigger="click" tooltip-placement="right" tooltip-append-to-body="true">!</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-lg-3 control-label">NPI Number <span class="mandatoryField">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name = "UsrDoctorsMaster[Nip_Number]" class="form-control" placeholder="HCP's NPI Number" required="" >
                                <span class="errorIcon "  tooltip="" tooltip-trigger="click" tooltip-placement="right" tooltip-append-to-body="true">!</span>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-sm-3 col-md-3 col-lg-3 control-label">Email Address <span class="mandatoryField">*</span></label>
                            <div class="col-sm-9">
                                <input type="email"  class="form-control " name = "UsrUsersMaster[User_Email]" placeholder="Email Address" autocomplete="off"  required="">
                                <span class="errorIcon "tooltip="" tooltip-trigger="click" tooltip-placement="right" tooltip-append-to-body="true">!</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-lg-3 control-label">Mobile</label>
                            <div class="col-sm-9">
                                <input type="text" name = "UsrDoctorsMaster[Doctor_Mobile]" class="form-control " placeholder="HCP's mobile number" >
                            </div>
                        </div>
                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-lg-3 control-label">Address 1</label>
                                <div class="col-sm-9">
                                    <input type="text"  name = "UsrDoctorsMaster[Doctor_Address_Line_1]"class="form-control " placeholder="Enter Line 1 of HCP's address" >
                                </div>
                            </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-lg-3 control-label">Address 2</label>
                            <div class="col-sm-9">
                                <input type="text" name = "UsrDoctorsMaster[Doctor_Address_Line_2]" class="form-control " placeholder="Enter Line 2 of HCP's address" >
                            </div>
                            </div>
                        <div class="form-group" >
                            <label class="col-sm-3 col-md-3 col-lg-3 control-label">City </label>
                            <div class="col-sm-9">
                                <input type="text" name = "UsrDoctorsMaster[Doctor_City]"  class="form-control " placeholder="Enter the HCP's city" >
                               

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 col-lg-3 control-label">State</label>
                            <div class="col-sm-9">
                                <input type="text" name = "UsrDoctorsMaster[Doctor_State]" class="form-control " placeholder="Enter the HCP's state" >
                               
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-sm-3 col-md-3 col-lg-3 control-label">Zip</label>
                            <div class="col-sm-9">
                                <input type="text" name = "UsrDoctorsMaster[Doctor_ZIP]" class="form-control " placeholder="Enter the HCP's ZIP code" >

                            </div>
                        </div>    
            </div><!-- body close -->
        <div class="modal-footer">
         <button type="button" class="btn" data-dismiss="modal">close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
         </div><!-- footer close -->
        </form>
      </div>
      
    </div>
  </div>
  
</div>


</body><!--Body end-->


