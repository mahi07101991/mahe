<?php
/**
 * Created by PhpStorm.
 * User: Mahesh
 * Date: 08-04-2015
 * Time: 14:42
 */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
$this->registerJsFile('listfilter/dist/list.js');
$this->registerJsFile('listfilter/search.js');
$this->registerJsFile('listfilter/filter.css');
?>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <style>
        #box1{
            border:1px solid #8457a7;
            height:140px;
            overflow: auto;
        }
        #box2{
            margin-top:0px;
            border:1px solid #8558a8;
            height:500px;px;
            width:565px;

        }
        h5{
            margin-top:10px;
            padding:9px;
            margin-left: 20px;;
            color:white;

            height:40px;
            width:520px;
            margin-top:-1px;
            text-align: justify;
        }
        #master{
            width:520px;
            height:412px;

            margin:0px auto;

        }
        .heading{
            background-color: #8457a7;
        }
        #new_brand{
            font-size: medium;
            color: white;
            padding:10px;
            text-decoration: none;
            height:40px;
            margin-left: 120px;
             background-color:#8457a7;/*

            background: -webkit-linear-gradient(left, rgba(255,0,0,0), rgba(255,255,255,255)); 
            background: -o-linear-gradient(right, rgba(255,0,0,0), rgba(255,255,255,255)); 
            background: -moz-linear-gradient(right, rgba(255,0,0,0), rgba(255,255,255,255)); 
            background: linear-gradient(to right, rgba(255,0,0,0), rgba(255,255,255,255)); */

        }
        #new_brand:hover{
            background-color:#8457a7;
            color:white;
        }
        #create-brand{

            height:40px;
            background-color:#8457a7;
        }
        .row-field{
            border-left: 5px solid #8457a7;
        }
        body {overflow-x:hidden;
                overflow-y:hidden;
                  }
        #update{
            margin-left:260px;
            margin-top:20px;
            border:0px;
            color:white;  
            background-color: #bf9dd9;
            padding:5px;
        }
        #delete{
            padding:5px;
            border:0px; 
            color:white; 
            background-color: #bf9dd9;
                }

        #clear{
        padding:5px;
        color:white;
        background-color: #bf9dd9;
        border:0px;
        }
        #saved{

            margin-left:260px;
            margin-top:20px;
            border:0px;
            color:white;  
            background-color: #bf9dd9;
            padding:5px;
        }
        tr th a{
            color: #8457a7;;
        }
        th{
            text-align: center;
        }

    </style>
    <script type="text/javascript">

        function getAdministrator(id,fname,lname,email,password){
            document.getElementById('hid').value=id;
            document.getElementById('firstname').value=fname;
            document.getElementById('lastname').value=lname;
            document.getElementById('email').value=email;
            document.getElementById('password').value=password;
                $("#update").show();
                $("#delete").show();
                $("#saved").hide();
                 $("#update").attr('style','background-color:#8457a7;')
        }
        function setsaved(){

            var id=document.getElementById("hid").value;
            var fname=document.getElementById("firstname").value;
            var lname=document.getElementById("lastname").value;
            var email=document.getElementById("email").value;
            var password=document.getElementById("password").value;

            if(fname=="" || lname=="" || email=="" || password=="")
                alert("Please enter Details");
            else{
            $.ajax({
                type:'POST',
                url: 'index.php?r=usr-users-master/setsaved',
                data: {'type':3,'f':fname,'l' :lname,'e' :email,'p' :password},
                success:function(data){
                    /*if(data[0]==1)*/
                    /*var result=JSON.parse(data);
                     document.getElementById("project-end-date").innerHTML = result['finaldate'];*/
                    location.reload();
                },
                error:function(){
                    alert("error");
                }
            });
            }
        }
            
         $(document).ready(function(){
            $("#saved").hide();

            $('#update').click(function(){
            var id=document.getElementById("hid").value;
            var fname=document.getElementById("firstname").value;
            var lname=document.getElementById("lastname").value;
            var email=document.getElementById("email").value;
            var password=document.getElementById("password").value;
             if(fname=="" || lname=="" || email=="" || password=="")
                alert("Please enter Details");
            else{
            $.ajax({
                type:'POST',
                url: 'index.php?r=usr-users-master/setupdate',
                data: {'id':id,'f':fname,'l':lname,'e':email,'p':password,},
                success:function(data){

                    /*if(data[0]==1)*/
                    /*var result=JSON.parse(data);
                     document.getElementById("project-end-date").innerHTML = result['finaldate'];*/
                    location.reload();
                },
                error:function(){
                    alert("error");
                }
            });
            }
            });
            
            $("#new_brand").click(function(){
                $("#update").hide();
                $("#delete").hide();
                $("#saved").show();
                 $("#saved").attr('style','background-color:#8457a7;')
            });
            $("#clear").click(function(){
               document.getElementById("brandname").value="";

            });
           
            $('#delete').click(function(){
                var id=document.getElementById("hid").value;
                $.ajax({
                type:'POST',
                url: 'index.php?r=usr-users-master/setdelete',
                data: {'id':id},
                success:function(data){
                    /*if(data[0]==1)*/
                    
                    /*var result=JSON.parse(data);
                     document.getElementById("project-end-date").innerHTML = result['finaldate'];*/
                    location.reload();
                },
                error:function(){
                    alert("error");
                }
            });
            });
        });
         
    </script>
</head>
<body>
<div id="container ">
        <div class="row">

            <div class="col-lg-4" style="height:500px;border: 1px solid #8457a7;overflow:auto;">
                <div id="create-brand">
                    
                     <button  id="new_brand" type="button" style="position:relative;margin-left:95px;border:0px;" class="btn btn-default" aria-label="Left Align">
                            <span class="glyphicon glyphicon-plus align-Scenter"  style="border:1px solid white;color: #8457a7;background:white;width:18px;height:18px;" ></span>
                            <strong>New Administrator</strong>
                            <span class="plus-sign-rounded "></span>
                    </button>
                </div>
                <div id="admin-users">
                      <input class="search col-lg-12" style="margin-top:5px;" placeholder=" Brand Search . . ." />
                      
                      <table class="table table-striped table-bordered">
                        <!-- IMPORTANT, class="list" have to be at tbody -->
                        <tbody class="list">
                            <?php
                                $r="";
                                for($i=0;$i<count($query);$i++)
                                {   $id=$query[$i]['User_Id'];
                                    $fname='"'.$query[$i]['User_First_Name'].'"';
                                    $lname='"'.$query[$i]['User_Last_Name'].'"';
                                    $email='"'.$query[$i]['User_Email'].'"';
                                    $password='"'.$query[$i]['password'].'"';
                                $r.="<tr onclick='getAdministrator($id,$fname,$lname,$email,$password)'".'><td class="admin">'.$query[$i]['User_First_Name'].'</td></tr>';
                                }   
                            echo $r;
                            ?>  
                        </tbody>
                      </table>
                
            </div>
            </div>

        <div class="col-lg-8">
                    <div id="box2">
            <div class="heading"><h5>user details</h5></div>
            <div class="container">
                <div class="row" style="margin-top: 10px;">
                    <label class="col-xs-2 col-sm-2 col-md-2 col-lg-2"  style="margin-left: -25px;text-align: right;">First Name<span style="color:red;">*</span></label>
                    <div class="form-group">
                        <input class="col-xs-3 col-sm-3 col-md-3 col-lg-3 " type="text" id="firstname" placeholder="First name if the Marketing Rep"/>
                    </div>
                </div>
                <div class="row" style="margin-top: 10px;">
                    <label class="col-xs-2 col-sm-2 col-md-2 col-lg-2"   style="margin-left: -25px;text-align: right;">Last Name</label>
                    <div class="form-group">

                        <input type="text" class="col-xs-3 col-sm-3 col-md-3 col-lg-3 " id="lastname"placeholder="Last name of the Marketing Rep"/>

                    </div>
                </div>
                <div class="row" style="margin-top: 10px;">
                    <label class="col-xs-2 col-sm-2 col-md-2 col-lg-2"   style="margin-left: -25px;text-align: right;">Email<span style="color:red;">*</span></label>
                    <div class="form-group">

                        <input type="email" class="col-xs-3 col-sm-3 col-md-3 col-lg-3 " id="email" placeholder="Marketing Rep's email"/>

                    </div>
                </div>
                <div class="row" style="margin-top: 10px;">
                    <label class="col-xs-2 col-sm-2 col-md-2 col-lg-2"   style="margin-left: -25px;text-align: right;">Password<span style="color:red;">*</span></label>
                    <div class="form-group">

                        <input type="text" class="col-xs-3 col-sm-3 col-md-3 col-lg-3 " id="password" placeholder="Enter a password for the Marketing Rep"/>

                    </div>
                </div>
                <div>
                    <input type="button"  id="update" value="Update">
                    <input type="button"  id="delete" value="Delete">
                    <input type="button"  id="saved"   onclick="setsaved()" value="Save">
                    <input type="button"  id="clear"  value="Clear">
                    <input type="hidden" id="hid" />
                </div>
            </div>
        </div><!--end of box2 -->

    </form>
</div>


        </div><!--row ending -->
</div>
</body><!--Body end-->

