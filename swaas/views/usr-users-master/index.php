<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsrUsersMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usr Users Masters';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usr-users-master-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Usr Users Master', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'User_Id',
            'Union_User_Id',
            'User_First_Name',
            'User_Last_Name',
            'User_Email:email',
            // 'User_Type',
            // 'Update_On',
            // 'Is_Deleted',
            // 'password',
            // 'Is_Optedout',
            // 'Optedout_Date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
