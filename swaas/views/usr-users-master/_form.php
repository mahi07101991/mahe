<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsrUsersMaster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usr-users-master-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Union_User_Id')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'User_First_Name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'User_Last_Name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'User_Email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'User_Type')->textInput() ?>

    <?= $form->field($model, 'Update_On')->textInput() ?>

    <?= $form->field($model, 'Is_Deleted')->textInput() ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'Is_Optedout')->textInput() ?>

    <?= $form->field($model, 'Optedout_Date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
