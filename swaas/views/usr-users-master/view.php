<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UsrUsersMaster */

$this->title = $model->User_Id;
$this->params['breadcrumbs'][] = ['label' => 'Usr Users Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usr-users-master-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->User_Id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->User_Id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'User_Id',
            'Union_User_Id',
            'User_First_Name',
            'User_Last_Name',
            'User_Email:email',
            'User_Type',
            'Update_On',
            'Is_Deleted',
            'password',
            'Is_Optedout',
            'Optedout_Date',
        ],
    ]) ?>

</div>
