<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsrUsersMasterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usr-users-master-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'User_Id') ?>

    <?= $form->field($model, 'Union_User_Id') ?>

    <?= $form->field($model, 'User_First_Name') ?>

    <?= $form->field($model, 'User_Last_Name') ?>

    <?= $form->field($model, 'User_Email') ?>

    <?php // echo $form->field($model, 'User_Type') ?>

    <?php // echo $form->field($model, 'Update_On') ?>

    <?php // echo $form->field($model, 'Is_Deleted') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'Is_Optedout') ?>

    <?php // echo $form->field($model, 'Optedout_Date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
