<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\url;

/* @var $this \yii\web\View */
/* @var $content string */
$this->registerCssFile('css/main/main.css');

?>
<?php $this->beginPage() ?>
    <?php $this->head() ?>

    <style type="text/css">
        .library{
            background-image:url('../../web/mages/library.png');
        }
        nav {
            width:0px;
            height:0px;
        }
    </style>

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap" >
    <?php
    NavBar::begin([

        'brandUrl' => Yii::$app->homeUrl,
    ]);
    echo Nav::widget(
        [
            'options' => ['class' => ' navbar-nav navbar-left tap-header'],
        ]
    );
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
