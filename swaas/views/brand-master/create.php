<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BrandMaster */

$this->title = 'Create Brand Master';
$this->params['breadcrumbs'][] = ['label' => 'Brand Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-master-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
