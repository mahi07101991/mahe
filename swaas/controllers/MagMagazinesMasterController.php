<?php

namespace app\controllers;

use Yii;
use app\models\MagMagazinesMaster;
use app\models\MagMagazinesMasterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MagMagazinesMasterController implements the CRUD actions for MagMagazinesMaster model.
 */
class MagMagazinesMasterController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MagMagazinesMaster models.
     * @return mixed
     */

    public function actionIndex()
    {
        $searchModel = new MagMagazinesMasterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionGetallmagazines(){
        $magazines = (new \yii\db\Query())
            ->select('*')
            ->from('mag_magazines_master')
            ->Where('Author_User_Id=:id',[':id'=>Yii::$app->user->identity->User_Id])
            ->all();
        return json_encode($magazines,true);
    }
    public function actionSetmagazines(){
        $model = new MagMagazinesMaster();
        $model->Magazine_Name = $_POST['magazine'];
        $model->Magazine_Product = $_POST['product'];
        $model->Magazine_Description = $_POST['desc'];
        $model->Author_User_Id = Yii::$app->user->identity->User_Id;
        $model->Magazine_Thumbnail = $_FILES["image"]["name"]; 
        if ($model->save()) {
        $fileName = $_FILES["image"]["name"]; 
        $fileTmpLoc = $_FILES["image"]["tmp_name"];
        $pathAndName = "upload/".$fileName;
        $moveResult = move_uploaded_file($fileTmpLoc, $pathAndName);
        }
        return json_encode($moveResult,true);
    }
    public function actionGetmagazines(){
        $query = (new \yii\db\Query())
            ->select('*')
            ->from('mag_magazines_master')
            ->where('Author_User_Id='.Yii::$app->user->identity->User_Id)
            ->all();
        /*print_r($query)or die;*/
        return json_encode($query,true);
    }
    /**
     * Displays a single MagMagazinesMaster model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MagMagazinesMaster model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatemagazine(){
        $model = new MagMagazinesMaster();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return 'alert("not sucess")';
        }
    }
    public function actionCreate()
    {
        $model = new MagMagazinesMaster();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MagMagazinesMaster model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MagMagazinesMaster model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MagMagazinesMaster model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MagMagazinesMaster the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MagMagazinesMaster::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
