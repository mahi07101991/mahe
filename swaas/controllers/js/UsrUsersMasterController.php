<?php

namespace app\controllers;

use Yii;
use app\models\UsrUsersMaster;
use app\models\UsrUsersMasterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsrUsersMasterController implements the CRUD actions for UsrUsersMaster model.
 */
class UsrUsersMasterController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsrUsersMaster models.
     * @return mixed
     */

    public function actionIndex()
    {
        $searchModel = new UsrUsersMasterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     public function actionSetsaved(){
        $fname = $_POST["f"];
        $lname = $_POST["l"];
        $email = $_POST["e"];
        $pass = $_POST["p"];
        $type = $_POST["type"];
        $query = Yii::$app->db->createCommand('INSERT INTO `usr_users_master`(`User_First_Name`, `User_Last_Name`, `User_Email`, `password`,`User_Type`) VALUES (:fname,:lname,:email,:pass,:type)',
                                                [':fname'=>$fname,':lname'=>$lname,':email'=>$email,':pass'=>$pass,':type'=>$type])->execute(); 
        return json_encode($query,true);
    }
    public function actionSetupdate()
    {
        $id = $_POST['id'];
        /*$id = json_decode($id);*/
        $fname = $_POST['f'];
        $lname = $_POST['l'];
        $email = $_POST['e'];
        $pass = $_POST['p'];
        /*$name = json_decode($name);*/
        $query =Yii::$app->db->createCommand('UPDATE  usr_users_master SET User_First_Name=:fname,User_Last_Name=:lname,User_Email=:email,password=:pass WHERE User_Id=:id',[':fname'=>$fname,':pass'=>$pass,':lname'=>$lname,':email'=>$email,':id'=>$id])->execute();
        return json_encode($query,true);
    }

    public function actionSetdelete()
    {
        $id = $_POST['id'];
        /*$id = json_decode($id);*/
        /*$name = json_decode($name);*/
        $query =Yii::$app->db->createCommand('DELETE  from usr_users_master  WHERE User_Id=:id',[':id'=>$id])->execute();
        return json_encode($query,true);
    }
     
    
     public function actionAdministrators()
    { 

        $searchModel = new UsrUsersMasterSearch();
        $dataProvider = $searchModel->searchAdmin(Yii::$app->request->queryParams);
        $query =  $query=(new \yii\db\Query())
        ->select('*')
        ->from('usr_users_master')
        ->where('User_Type=3')
        ->all();
        return $this->render('administrators', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'query'=>$query,
        ]);
    }
    

    /**
     * Displays a single UsrUsersMaster model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UsrUsersMaster model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UsrUsersMaster();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->User_Id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UsrUsersMaster model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->User_Id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UsrUsersMaster model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UsrUsersMaster model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UsrUsersMaster the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UsrUsersMaster::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
