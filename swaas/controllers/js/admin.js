
function readURL_Magazine(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#magazine-image-preview').html('<img id="preMagazine"class="img-responsive" width="100px" height="100px">').show('flip',3000);
        reader.onload = function(e) {
            $('#preMagazine').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL_Article(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#article-image-preview').html('<img id="preArticle" class="img-responsive" width="100px" height="100px">').show('flip',3000);
        reader.onload = function(e) {
            $('#preArticle').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function getarticle() {
    var selectBox = document.getElementById("selected-article");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    alert(selectedValue);
}

function setArticle(){
    var article = document.getElementById("articleName").value;
    var node= document.createElement('tr');
    node.innerHTML = '<td>'+article+'</td><td><button class="btn btn-default" type="button">Publish</td>';
    document.getElementById("Afield2").appendChild(node);
}

function setarticleToMagazine(){
    var name = document.getElementById("articleName").value;
    var desc = document.getElementById("articleDesc").value;
    var magazine_id = document.getElementById("getingMagazine").value;
    var input = document.getElementById("article-thumbnail");
    file = input.files[0];
    if(file != undefined){
        formData= new FormData();
        if(!!file.type.match(/image.*/)){
            formData.append("image", file);
            formData.append("name",name);
            formData.append("desc",desc);
            formData.append("magazineid",magazine_id);
            $.ajax({
                url: 'index.php?r=mag-articles-master/setarticlestodb',
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    callAllArticles();
                }
            });
        }else{
            alert('Not a valid image!');
        }
    }else{
        alert('Input something!');
    }
}
function callAllArticles(){
        $.ajax({
        url: 'index.php?r=mag-articles-master/getallarticles',
        success:function(data){
            var temp='';
            var data = JSON.parse(data);
            for(var i=0;i<data.length;i++)
            {
                temp += '<div class="row" style="margin-bottom: 3px;border-left:5px solid #5d8a8b;border-top:2px solid #d6d7cf;border-right: 2px solid #d6d7cf;border-bottom:2px solid #d6d7cf;">' +
                            '<div class="col-lg-9">' +
                                '<div class="row">'+
                                    '<div>'+
                                        '<img style="border-left:8px solid black;round:gray;margin-top:4px;margin-left:3px;" src="article/'+data[i].Article_Thumbnail+'" width="50px" height="50px;">'+
                                        '<span style="margin-left:7px">'+data[0].Article_Name+'</span>'+
                                    '</div>'+
                                    '<div>'+
                                        '<article>' +
                                            '<h5 style="color:dimgrey;">Description:</h5>'+
                                            '<p style="font-size: 80%;margin-left:5px;">'+'&nbsp&nbsp&nbsp&nbsp<i>'+data[i].Article_Description+'  !!</i></p>'+
                                        '</article>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-lg-3" >' +
                                '<button type="button" style="margin-left:22px;border-radius:0px;border-top:0px;border-right:0px;border-bottom-left-radius:10px;border-left:3px solid #5D8A8B;border-bottom:3px #5D8A8B;background-color:black;color:white;" class="btn btn-default" onclick="publishToDoctors('+data[i].id+')">Publish</button>'+
                            '</div>' +
                        '</div>';
            }
            $("#displayAllArticles").html(temp);
        },
        error:function(){
            alert("error");
        }
    });
}
function sendMagazineToController() {
    $("#preMagazine").detach();
    var magazine = document.getElementById("Magazine_Name").value;
    var product = document.getElementById("select-brand").value;
    var desc = document.getElementById("Magazine_Description").value;
    var input = document.getElementById("magazine-thumbnail");
  file = input.files[0];
  if(file != undefined){
    formData= new FormData();
    if(!!file.type.match(/image.*/)){
      formData.append("image", file);
      formData.append("magazine",magazine);
      formData.append("product",product);
      formData.append("desc",desc);
      $.ajax({
        url: "index.php?r=mag-magazines-master/setmagazines",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            callAllMagazines();
        }
      });
    }else{
      alert('Not a valid image!');
    }
  }else{
    alert('Input something!');
  }
}

function getArticlesForMagazine(id){
    $.ajax({
        url: 'index.php?r=mag-articles-master/getarticlesformagazine',
        data:{'id':id},
        success:function(data){
            alert(data);

        },
        error:function(){
            alert("error");
        }
    });
}

/*CALL ALL MAGAZINES*/
function callAllMagazines(){
    $.ajax({
        url: 'index.php?r=mag-magazines-master/getallmagazines',
        success:function(data){
            var temp='';
            var data = JSON.parse(data);
            for(var i=0;i<data.length;i++)
            {
                temp += '<div class="row" style="margin-bottom: 3px;border-left:5px solid #5d8a8b;border-top:2px solid #d6d7cf;border-right: 2px solid #d6d7cf;border-bottom:2px solid #d6d7cf;">' +
                            '<div class="col-lg-4">' +
                                '<h5 style="padding:2px;background-color:#d75e2c;color:white;border-bottom-left-radius:15px;border-top-right-radius:15px;margin-left:-20px;margin-left:5px;text-align: center" class="marquee"> ' + data[i].Magazine_Name + '</h5>' +
                                '<img style="border-left:8px solid black;round:gray;" src="upload/'+data[i].Magazine_Thumbnail+'" width="100px" height="100px;">'+
                                '<div>' +
                                    '<button type="button" class="btn btn-default" style="margin-top:5px;margin-bottom:3px;margin-left:11px;"><small>Add Topic</small></button>' +
                                '</div>' +
                            '</div>'+
                            '<div class="col-lg-8">' +
                            '</div>' +
                        '</div>';
            }
            $("#displayAllMagazines").html(temp);
        },
        error:function(){
            alert("error");
        }
    });
}
$(document).ready(function(){
    var flag1= 1,flag2= 0,flag3= 0;
    callAllArticles();
    callAllMagazines();
    $('#create-magazine').on('click', function () {
        $.ajax({
            url: 'index.php?r=brand-master/getbrands',
            success:function(data){
                var temp=null;
                var data = JSON.parse(data);
                for(var i=0;i<data.length;i++)
                temp = temp+'<option value='+data[i].id+'>'+data[i].name+'</option>';
                $("#select-brand").html(temp);
                $('#createMModal').modal('show');
            },
            error:function(){
                alert("error");
            }
        });
    });
    $('#create-article').on('click', function () {
        $.ajax({
            url: 'index.php?r=mag-magazines-master/getmagazines',
            success:function(data){
                var temp=null;
                var data = JSON.parse(data);
                for(var i=0;i<data.length;i++)
                    temp = temp+'<option value='+data[i].id+'>'+data[i].Magazine_Name+'</option>';
                $("#getingMagazine").html(temp);
                $('#myModal').modal('show');
            },
            error:function(){
                alert("error");
            }
        });
    });

    $("#magazine-thumbnail").on('change',function(){
        readURL_Magazine(this);
    });
    $("#article-thumbnail").on('change',function(){
        readURL_Article(this);
    });

    $('#update-magazine').on('click', function () {
        $('#deleteMModal').modal('show');
    });

    $('#delete-magazine').on('click', function () {
        $('#deleteMModal').modal('show');
    });
    $('#create-session').on('click', function () {
        $('#createSModal').modal('show');
    });

    $('#update-session').on('click', function () {
        $('#deleteSModal').modal('show');
    });

    $('#delete-session').on('click', function () {
        $('#deleteSModal').modal('show');
    });



    $('#delete-article').on('click', function () {
        $('#deleteModal').modal('show');
    });
    $('#update-article').on('click', function () {
        $('#updateModal').modal('show');
    });
    $('#deleteArticle').on('click', function () {
        alert("deleted sucessfully");
    });
    $("#pannel-p1").on('click' ,function() {
       $("#pannel-p1").css({'border-left':'7px solid #c1dd79'});
       $("#pannel-p2").css({'border-left':'7px solid orange'});
       $("#pannel-p3").css({'border-left':'7px solid orange'});

        /*$("#article-heading").append('<span id="article-span-heading"><span><a href="#" style="color:white;" id="create-article">CREATE</a></span>' +
        '<span><a href="#" style="color:white;" id="create-article"> EDIT</a></span></span>');
        $("#magazine-span-heading").detach();
        $("#session-span-heading").detach();*/
   });
    $("#pannel-p2").on('click' ,function() {
       $("#pannel-p1").css({'border-left':'7px solid orange'});
       $("#pannel-p2").css({'border-left':'7px solid #c1dd79'});
       $("#pannel-p3").css({'border-left':'7px solid orange'});
        /*$("#magazine-heading").append('<span id="magazine-span-heading"><span><a href="#" style="color:white;" id="create-article">CREATE</a></span>' +
        '<span><a href="#" style="color:white;" id="magazine-article"> EDIT</a></span></span>');
        $("#article-span-heading").detach();
        $("#session-span-heading").detach();*/
    });
    $("#pannel-p3").on('click' ,function() {
       $("#pannel-p1").css({'border-left':'7px solid orange'});
       $("#pannel-p2").css({'border-left':'7px solid orange'});
       $("#pannel-p3").css({'border-left':'7px solid #c1dd79'});
        /*$("#session-heading").append('<span id="session-span-heading"><span><a href="#" style="color:white;" id="create-article">CREATE</a></span>' +
        '<span><a href="#" style="color:white;" id="create-article"> EDIT</a></span></span>');
        $("#article-span-heading").detach();
        $("#magazine-span-heading").detach();*/
    });
});

