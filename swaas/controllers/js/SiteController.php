<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Hcsregistration;
class SiteController extends Controller
{
     
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

      public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->goHome();
        }
        else{
        
                if($model->load(Yii::$app->request->post())) 
                {
                    $query = (new \yii\db\Query())
                    ->select('User_Type,User_Email')
                    ->from('Usr_Users_Master')
                    ->where('User_Email=:email',[':email'=>$model->username])->one();
                    if($query['User_Type']==0 || $query['User_Type']==1 || $query['User_Type']==2 || $query['User_Type']==3)
                    {   
                        $this->layout='main3';
                        return $this->render('login',['model'=>$model,'setpassword'=>$query]);
                    }
                    if($query['User_Type']==4)
                    {
                    $query2 = (new \yii\db\Query())
                    ->select('password,User_Type')
                    ->from('Usr_Users_Master')
                    ->where('User_Email=:email',[':email'=>$model->username])->one();
                        $model->password=$query2['password'];
                        $model->login();
                        /*$invalid=['invalid'=>'invalid'];*/
                        return $this->goBack();
                    }
                    $model->login();
                    /* $invalid=['invalid'=>'invalid'];*/
                    return $this->goBack();
                }
                else {
                        $this->layout='main3';
                        return $this->render('login', [
                        'model' => $model,'setpassword'=>null,
                    ]);
                }
        }
    }

    public function actionIndex()
    {

            return $this->render('index');

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->layout='main3';
        return $this->render('login',['setpassword'=>null]);
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
?>