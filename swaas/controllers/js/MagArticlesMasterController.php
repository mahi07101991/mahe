<?php

namespace app\controllers;

use Yii;
use app\models\MagArticlesMaster;
use app\models\MagArticlesMasterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MagArticlesMasterController implements the CRUD actions for MagArticlesMaster model.
 */
class MagArticlesMasterController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MagArticlesMaster models.
     * @return mixed
     */

    public function actionSetarticlestodb(){
        $model = new MagArticlesMaster();
        $model->Article_Name = $_POST['name'];
        $model->Article_Description = $_POST['desc'];
        $model->Magazine_Id= $_POST['magazineid'];
        $model->Article_Thumbnail = $_FILES["image"]["name"]; 
        $model=$model->save(); 
        if($model)
        {
        $fileName = $_FILES["image"]["name"]; 
        $fileTmpLoc = $_FILES["image"]["tmp_name"];
        $pathAndName = "article/".$fileName;
        $moveResult = move_uploaded_file($fileTmpLoc, $pathAndName);
            $model="sucessfully created";
        }
        return json_encode($model,true);
    }
    public function actionGetallarticles(){
        $query = (new \yii\db\Query())
            ->select('*')
            ->from('mag_articles_master')->all();
        /*print_r($query)or die;*/
        return json_encode($query,true);
    }
    public function actionIndex()
    {
        $searchModel = new MagArticlesMasterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MagArticlesMaster model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MagArticlesMaster model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MagArticlesMaster();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MagArticlesMaster model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MagArticlesMaster model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MagArticlesMaster model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MagArticlesMaster the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MagArticlesMaster::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
