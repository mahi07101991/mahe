
tinyMCE.init({
        selector: "#textarea1",
        plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
        ],
         toolbar1: " undo redo | cut copy paste | bullist numlist |  outdent indent blockquote | link unlink image |table | hr removeformat | subscript superscript | print fullscreen | table",
         toolbar2: "variables | images ",
         menubar:false,
         setup: function(editor){
		        editor.addButton('variables',
		        {
		            text: 'Variables',
		            type: 'menubutton',
		            icon: false,
		            menu: [
		                {
		                    text: 'DoctorName',
		                    onclick: function(){
                                tinyMCE.activeEditor.setContent("{Doctor Name}");
                            }
		                },
		                {
		                    text: 'Menu-2',
		                    onclick: function(){
		                        alert("Clicked on Menu-2.");
		                    }
		                }
		            ]
		        });
		        		        editor.addButton('images',
		        {
		            text: 'Images',
		            type: 'menubutton',
		            icon: false,
		            menu: [
		                {
		                    text: 'Email Template Image',
		                    onclick: function(){
		                        alert("Clicked on Menu-1.");
		                    }
		                },
		                {
		                    text: 'Menu-2',
		                    onclick: function(){
		                        alert("Clicked on Menu-2.");
		                    }
		                }
		            ]
		        });

    }

       
       
});
