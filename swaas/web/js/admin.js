
function readURL_Magazine(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#magazine-image-preview').html('<img id="preMagazine" class="img-responsive" width="100px" height="100px">').show('flip', 3000);
        reader.onload = function(e) {
            $('#preMagazine').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL_Article(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#article-image-preview').html('<img id="preArticle" class="img-responsive" width="100px" height="100px">').show('flip',3000);
        reader.onload = function(e) {
            $('#preArticle').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function deleteArticle() {
    var id = document.getElementById("delete-list-for-articles").value;
    /*var id = deleteArticle.options[id.selectedIndex].value;*/
    /*alert(id);*/
    $.ajax({
        type: 'POST',
        url: 'index.php?r=mag-articles-master/deletearticlebyid',
        data: {'id': id},
        success: function (data) {
            /*      alert("deleted sucessfully");*/
            groupArticles();
        },
        error: function () {
            alert("error");
        }
    });

}
function groupArticles(){
    document.getElementById("displayAllArticles").innerHTML = '';
    $.ajax({
        url: 'index.php?r=mag-articles-master/articegroups',
        success:function(data){
            if(data.length!=0)
            {
                data = JSON.parse(data);
                magazines_length = data.magazines.length;
                articles_length = data.articles.length;
                /*alert(magazines_length);
                 alert(articles_length);*/
                for(var i=0;i<magazines_length;i++)
                {
                    /*alert(data.magazines[i].id);
                     alert(data.articles[i].Magazine_Id);*/
                    var count = 0;
                    for(var j=0;j<articles_length;j++)
                    {
                        if(data.magazines[i].id == data.articles[j].Magazine_Id){
                            ++count;
                        }

                    }
                    if (count > 0)
                        createGroupForArticles(count, data.magazines[i].id, data.magazines[i].Magazine_Thumbnail, data.magazines[i].Magazine_Description,data.magazines[i].Magazine_Name);

                    /* alert(data.magazines[i].Magazine_Name+' has this much of articles '+temp);*/
                }
            }else{
                $("#displayAllArticles").append("No data found please contact admin for any issues").fadeIn(2000);
            }

        },
        error:function(){
            alert("error");
        }
    });
}

function createGroupForArticles(total,magazine_id,imagefile,desc,name){
    var temp1 = '';

    temp1 +='<div class="row" style="margin-bottom: 3px;border-left:5px solid #5d8a8b;border-top:2px solid #d6d7cf;border-right: 2px solid #d6d7cf;border-bottom:2px solid #d6d7cf;">' +
                    '<div class="col-lg-8">' +
                        '<div class="row">'+
                            '<div>'+
                                '<img class="imgz" src="upload/' + imagefile + '" width="50px" height="50px;">' +
                                '<span style="color:#0044cc;font-size: 200%;margin-top:-5px;">' + total + '</span>' +
                                '<span style="margin-left:7px">'+name+'</span>'+
                            '</div>'+
                            '<div>'+
                                '<article>' +
                                    '<h5>Description:</h5>'+
                                    '<p style="font-size: 80%;margin-left:5px;">'+'&nbsp&nbsp&nbsp&nbsp<i>'+desc+'  !!</i></p>'+
                                '</article>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
    '<div class="col-lg-4" style="display:inline">' +

    '<button type="button" class=" btn button-box" onclick="callArticlesByMagazineId(' + magazine_id + ')"">View Articles</button>' +
                    '</div>' +
                '</div>';
    $("#displayAllArticles").append(temp1).fadeIn(2000);
    /*$("#group-file-image").css('background-image',url);*/
}


function setarticleToMagazine(){
    var name = document.getElementById("articleName").value;
    var desc = document.getElementById("articleDesc").value;
    var magazine_id = document.getElementById("getingMagazine").value;
    var input = document.getElementById("article-thumbnail");
    file = input.files[0];
    if(file != undefined){
        formData= new FormData();
        if(!!file.type.match(/image.*/)){
            formData.append("image", file);
            formData.append("name",name);
            formData.append("desc",desc);
            formData.append("magazineid",magazine_id);
            $.ajax({
                url: 'index.php?r=mag-articles-master/setarticlestodb',
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    /*alert(data);*/
                    /*callAllArticles();*/
                    if(data.length!=0) {
                        groupArticles();
                    }
                },
                error: function () {
                    alert("error");
                }

            });
        }else{
            alert('Not a valid image!');
        }
    }else{
        alert('Input something!');
    }
}
function callArticlesByMagazineId(id) {

 $.ajax({
 type: 'POST',
 url: 'index.php?r=mag-articles-master/getarticlesbymagazineid',
 data: {'id': id},
 success: function (data) {
     /*alert(data);*/
     var temp='';
     data = JSON.parse(data);         /*temp = '<span class="glyphicon glyphicon-arrow-left" onclick=" groupArticles()" style="cursor:pointer" aria-hidden="true"></span>';*/
         for (var i = 0; i < data.length; i++) {
             temp += '<div class="row" style="margin-bottom: 3px;border-left:5px solid #5d8a8b;border-top:2px solid #d6d7cf;border-right: 2px solid #d6d7cf;border-bottom:2px solid #d6d7cf;">' +
             '<div class="col-lg-8">' +
             '<div class="row">' +
             '<div>' +
             '<img class="imgz1" src="article/' + data[i].Article_Thumbnail + '" width="50px" height="50px;">' +
             '<span style="margin-left:7px">' + data[0].Article_Name + '</span>' +
             '</div>' +
             '<div>' +
             '<article>' +
             '<h5 >Description:</h5>' +
             '<p style="font-size: 80%;margin-left:5px;">' + '&nbsp&nbsp&nbsp&nbsp<i>' + data[i].Article_Description + '  !!</i></p>' +
             '</article>' +
             '</div>' +
             '</div>' +
             '</div>' +
             '<div class="col-lg-4"  >' +
             '<button  type="button"  class="btn button-box" onclick="create_email_template(' + data[i].id + ')">Email Template</button>';
             if (data[i].Email_Template_Id >= 1) {
                 temp += '<button  type="button"  class="btn button-box" onclick="publishToDoctors(' + data[i].id + ')">Publish</button>' +
                 '<button  type="button"  class="btn button-box"" onclick="publishToDoctors(' + data[i].id + ')">Notify</button>';
             }
             temp += '</div>' +
             '</div>';
         }
     $("#panel-p1").css({'border-left':'7px solid #c1dd79'});
     $("#panel-p2").css({'border-left':'7px solid orange'});
     $("#panel-p3").css({'border-left':'7px solid orange'});
     $("#collapse2").removeClass("in");
     $("#collapse2").attr('aria-expanded', false);
     $("#collapse1").addClass("in");
     $("#collapse1").css('height','40%');
     $("#collapse1").attr('aria-expanded', true);
     $("#displayAllMagazines").html(temp).fadeIn(2000);
 }
 });
}
function create_email_template(id){
    document.getElementById("hidden-field").value=id;
    document.getElementById('template-subject').value = '';
    tinyMCE.activeEditor.setContent('');
    $.ajax({
        url: 'index.php?r=adm-email-templates/gettemplates',
        success:function(data){
            var temp=null;
                data = JSON.parse(data);
                for (var i = 0; i < data.length; i++)
                    temp = temp + '<option value="' + data[i].id + '"> ' + data[i].mail_template + '</option>';
                $("#select-template").html(temp);
                $('#CreateEmailTemplateModel').modal('show');

        },
        error:function(){
            alert("error");
        }
    });
}

function gettemplatedetails(){
    var selectBox = document.getElementById("select-template");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    /* alert(document.getElementById('textarea1').value);*/
    /*alert(selectedValue);*/
    $.ajax({
        type:'POST',
        url :'index.php?r=adm-email-templates/gettemplatevalues',
        data :{'id':selectedValue},
        success:function(data){
            var temp=null;
            /*alert(data);*/
                data = JSON.parse(data);
                document.getElementById('template-subject').value = data.mail_subject;
                tinyMCE.activeEditor.setContent(data.mail_body);
                //document.getElementById('textarea1').value = data.mail_body;

        },
        error:function(){
            alert("error");
        }
    });
}
function SendTemplateToController(){
    var selectBox = document.getElementById("select-template");
    var ids = selectBox.options[selectBox.selectedIndex].value;
    var subject = document.getElementById("template-subject").value;
    var articleid = document.getElementById("hidden-field").value;
    /*var body = tinyMCE.activeEditor('textarea1');*/
    var body = tinyMCE.activeEditor.getContent();
   /* alert(id);
    alert(subject);
    alert(body);*/
    $.ajax({
        type:'POST',
        url: 'index.php?r=mail-templates/createtemplate',
        data:{'ids':ids,'subject':subject,'body':body,'articleid':articleid},
        success:function(data){
        alert(data);
            var temp='';
            data = JSON.parse(data);

        },
        error:function(){
            alert("error");
        }
        }
    );}
function callAllArticles(){
        $.ajax({
        url: 'index.php?r=mag-articles-master/getallarticles',
        success:function(data){
            var temp='';

            data = JSON.parse(data);
            for(var i=0;i<data.length;i++)
            {
                temp += '<div class="row" style="margin-bottom: 3px;border-left:5px solid #5d8a8b;border-top:2px solid #d6d7cf;border-right: 2px solid #d6d7cf;border-bottom:2px solid #d6d7cf;">' +
                            '<div class="col-lg-8">' +
                                '<div class="row">'+
                                    '<div>'+
                                        '<img class="imgz1" src="article/'+data[i].Article_Thumbnail+'" width="50px" height="50px;">'+
                                        '<span style="margin-left:7px">'+data[0].Article_Name+'</span>'+
                                    '</div>'+
                                    '<div>'+
                                        '<article>' +
                                            '<h5>Description:</h5>'+
                                            '<p style="font-size: 80%;margin-left:5px;">'+'&nbsp&nbsp&nbsp&nbsp<i>'+data[i].Article_Description+'  !!</i></p>'+
                                        '</article>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-lg-4" >' +
                                '<button type="button"  class="btn button-box" onclick="publishToDoctors('+data[i].id+')">Publish</button>'+
                            '</div>' +
                        '</div>';
            }
                $("#displayAllMagazines").html(temp).fadeIn(2000);
        },
        error:function(){
            alert("error");
        }
    });
}
function sendMagazineToController() {
    $("#preMagazine").detach();
    var magazine = document.getElementById("Magazine_Name").value;
    var product = document.getElementById("select-brand").value;
    var desc = document.getElementById("Magazine_Description").value;
    var input = document.getElementById("magazine-thumbnail");
  file = input.files[0];
  if(file != undefined){
    formData= new FormData();
    if(!!file.type.match(/image.*/)){
      formData.append("image", file);
      formData.append("magazine",magazine);
      formData.append("product",product);
      formData.append("desc",desc);
      $.ajax({
        type:'POST',
        url: "index.php?r=mag-magazines-master/setmagazines",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            /*alert(data);*/
            callAllMagazines();
        }
      });
    }else{
      alert('Not a valid image!');
    }
  }else{
    alert('Input something!');
  }
}

/*CALL ALL MAGAZINES*/
function callAllMagazines(){
    $.ajax({
        url: 'index.php?r=mag-magazines-master/getallmagazines',
        success:function(data){
            var temp='';
            data = JSON.parse(data);
            for(var i=0;i<data.length;i++)
            {
                temp += '<div class="row" style="margin-bottom: 3px;border-left:5px solid #000000;border-top:2px solid #d6d7cf;border-right: 2px solid #d6d7cf;border-bottom:2px solid #d6d7cf;">' +
                            '<div class="col-lg-8">' +
                                '<div class="row">'+
                                    '<div>'+
                                        '<img class="imgz1" src="upload/'+data[i].Magazine_Thumbnail+'" width="50px" height="50px;">'+
                                        '<span style="margin-left:7px">'+data[0].Magazine_Name+'</span>'+
                                    '</div>'+
                                    '<div>'+
                                        '<article>' +
                                            '<h5>Description:</h5>'+
                                            '<p style="font-size: 80%;margin-left:5px;">'+'&nbsp&nbsp&nbsp&nbsp<i>'+data[i].Magazine_Description+'  !!</i></p>'+
                                            '<button type="button" class="btn btn-default" style="margin-top:5px;margin-bottom:3px;margin-left:11px;"><small>Add Topic</small></button>' +
                                        '</article>'+
                                    '</div>'+
                                '</div>' +
                            '</div>'+
                            '<div class="col-lg-4">' +
                                '<button type="button"  class="btn button-box" onclick="publishToDoctors('+data[i].id+')">Publish</button>'+
                            '</div>' +
                        '</div>';
            }
            $("#displayAllMagazines").html(temp).fadeIn(2000);
        },
        error:function(){
            alert("error");
        }
    });
}
$(document).ready(function(){
    /*var flag1= 1,flag2= 0,flag3= 0;*/
    callAllArticles();
    /*callAllMagazines();*/
    groupArticles();
    $('#create-magazine').on('click', function () {
        $.ajax({
            url: 'index.php?r=brand-master/getbrands',
            success:function(data){
                var temp=null;
                if(data.length!=0)
                {
                data = JSON.parse(data);
                for(var i=0;i<data.length;i++)
                temp = temp+'<option value='+data[i].id+'>'+data[i].name+'</option>';
                $("#select-brand").html(temp);
                $('#createMModal').modal('show');
                }else{
                    $("#select-brand").html("No data found please contact admin for any issues").fadeIn(2000);

                }
            },
            error:function(){
                alert("error");
            }
        });
    });
    $('#create-article').on('click', function () {
        $.ajax({
            url: 'index.php?r=mag-magazines-master/getmagazines',
            success:function(data){
                var temp=null;
                data = JSON.parse(data);
                for(var i=0;i<data.length;i++)
                    temp = temp+'<option value='+data[i].id+'>'+data[i].Magazine_Name+'</option>';
                $("#getingMagazine").html(temp);
                $('#myModal').modal('show');
            },
            error:function(){
                alert("error");
            }
        });
    });

    $("#magazine-thumbnail").on('change',function(){
        readURL_Magazine(this);
    });
    $("#article-thumbnail").on('change',function(){
        readURL_Article(this);
    });

    $('#update-magazine').on('click', function () {
        $('#deleteMModal').modal('show');
    });

    $('#delete-magazine').on('click', function () {

        $('#deleteMModal').modal('show');
    });
    $('#create-session').on('click', function () {
        $('#createSModal').modal('show');
    });

    $('#update-session').on('click', function () {
        $('#deleteSModal').modal('show');
    });

    $('#delete-session').on('click', function () {
        $('#deleteSModal').modal('show');
    });

    $('#delete-article').on('click', function () {
        $.ajax({
            url: 'index.php?r=mag-articles-master/getallarticles',
            success: function (data) {
                var temp = null;
                data = JSON.parse(data);
                for (var i = 0; i < data.length; i++)
                    temp = temp + '<option value="' + data[i].id + '"> ' + data[i].Article_Name + '</option>';
                if (temp != null) {
                    $("#delete-list-for-articles").html(temp);
                    $('#deleteModal').modal('show');
                }
                else
                    alert("No data Found Please refer admin");

            },
            error: function () {
                alert("error");
            }
        });


    });
    $('#update-article').on('click', function () {
        $.ajax({
            url: 'index.php?r=mag-articles-master/getallarticles',
            success: function (data) {
                var temp = null;
                data = JSON.parse(data);
                for (var i = 0; i < data.length; i++)
                    temp = temp + '<option value="' + data[i].id + '"> ' + data[i].Article_Name + '</option>';
                if (temp != null) {
                    $("#update-list-for-articles").html(temp);
                    $('#updateModal').modal('show');
                }
                else
                    alert("No data Found Please refer admin");

            },
            error: function () {
                alert("error");
            }

        });
    });
    $("#article-id").on('click' ,function() {
       $("#panel-p1").css({'border-left':'7px solid #c1dd79'});
       $("#panel-p2").css({'border-left':'7px solid orange'});
       $("#panel-p3").css({'border-left':'7px solid orange'});

        /*$("#article-heading").append('<span id="article-span-heading"><span><a href="#" style="color:white;" id="create-article">CREATE</a></span>' +
        '<span><a href="#" style="color:white;" id="create-article"> EDIT</a></span></span>');
        $("#magazine-span-heading").detach();
        $("#session-span-heading").detach();*/
   });
    $("#magazine-id").on('click' ,function() {
       $("#panel-p1").css({'border-left':'7px solid orange'});
       $("#panel-p2").css({'border-left':'7px solid #c1dd79'});
       $("#panel-p3").css({'border-left':'7px solid orange'});
        /*$("#magazine-heading").append('<span id="magazine-span-heading"><span><a href="#" style="color:white;" id="create-article">CREATE</a></span>' +
        '<span><a href="#" style="color:white;" id="magazine-article"> EDIT</a></span></span>');
        $("#article-span-heading").detach();
        $("#session-span-heading").detach();*/
    });
    $("#session-id").on('click' ,function() {
       $("#panel-p1").css({'border-left':'7px solid orange'});
       $("#panel-p2").css({'border-left':'7px solid orange'});
       $("#panel-p3").css({'border-left':'7px solid #c1dd79'});
        /*$("#session-heading").append('<span id="session-span-heading"><span><a href="#" style="color:white;" id="create-article">CREATE</a></span>' +
        '<span><a href="#" style="color:white;" id="create-article"> EDIT</a></span></span>');
        $("#article-span-heading").detach();
        $("#magazine-span-heading").detach();*/
    });
});

