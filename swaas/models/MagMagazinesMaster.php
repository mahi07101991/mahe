<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mag_magazines_master".
 *
 * @property integer $id
 * @property string $Magazine_Name
 * @property integer $Magazine_Product
 * @property resource $Magazine_Thumbnail
 * @property string $Updated_On
 * @property boolean $Is_Deleted
 * @property integer $Author_User_Id
 * @property string $Magazine_Description
 */
class MagMagazinesMaster extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mag_magazines_master';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Magazine_Product', 'Author_User_Id'], 'integer'],
            [['Magazine_Thumbnail'], 'string'],
            [['Updated_On'], 'safe'],
            [['Is_Deleted'], 'boolean'],
            [['Magazine_Name', 'Magazine_Description'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Magazine_Name' => 'Magazine  Name',
            'Magazine_Product' => 'Magazine  Product',
            'Magazine_Thumbnail' => 'Magazine  Thumbnail',
            'Updated_On' => 'Updated  On',
            'Is_Deleted' => 'Is  Deleted',
            'Author_User_Id' => 'Author  User  ID',
            'Magazine_Description' => 'Magazine  Description',
        ];
    }
}
