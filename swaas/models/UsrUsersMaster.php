<?php

namespace app\models;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;

use Yii;

/**
 * This is the model class for table "usr_users_master".
 *
 * @property integer $User_Id
 * @property string $Union_User_Id
 * @property string $User_First_Name
 * @property string $User_Last_Name
 * @property string $User_Email
 * @property integer $User_Type
 * @property string $Update_On
 * @property integer $Is_Deleted
 * @property string $password
 * @property integer $Is_Optedout
 * @property string $Optedout_Date
 */
class UsrUsersMaster extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usr_users_master';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Union_User_Id', 'User_First_Name', 'User_Last_Name', 'User_Email', 'User_Type', 'Update_On', 'Is_Deleted', 'password', 'Is_Optedout', 'Optedout_Date']],
            [['User_Type', 'Is_Deleted', 'Is_Optedout'], 'integer'],
            [['Update_On', 'Optedout_Date']],
            [['Union_User_Id', 'User_First_Name', 'User_Last_Name', 'User_Email', 'password'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'User_Id' => 'User  ID',
            'Union_User_Id' => 'Union  User  ID',
            'User_First_Name' => 'Users',
            'User_Last_Name' => 'User  Last  Name',
            'User_Email' => 'User  Email',
            'User_Type' => 'User  Type',
            'Update_On' => 'Update  On',
            'Is_Deleted' => 'Is  Deleted',
            'password' => 'Password',
            'Is_Optedout' => 'Is  Optedout',
            'Optedout_Date' => 'Optedout  Date',
        ];
    }
        /** INCLUDE USER LOGIN VALIDATION FUNCTIONS**/
        /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
/* modified */
    public static function findIdentityByAccessToken($token, $type = null)
    {
          return static::findOne(['access_token' => $token]);
    }
 
/* removed
    public static function findIdentityByAccessToken($token)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
*/
    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUser_Email($User_Email)
    {
        return static::findOne(['User_Email' => $User_Email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === sha1($password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Security::generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Security::generateRandomKey();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Security::generateRandomKey() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    /** EXTENSION MOVIE **/

}
