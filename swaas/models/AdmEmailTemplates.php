<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "adm_email_templates".
 *
 * @property integer $id
 * @property string $mail_template
 * @property string $mail_subject
 * @property string $mail_body
 */
class AdmEmailTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adm_email_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mail_template', 'mail_subject', 'mail_body'], 'required'],
            [['mail_body'], 'string'],
            [['mail_template', 'mail_subject'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mail_template' => 'Mail Template',
            'mail_subject' => 'Mail Subject',
            'mail_body' => 'Mail Body',
        ];
    }
}
