<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mag_articles_master".
 *
 * @property integer $id
 * @property string $Article_Name
 * @property integer $Unio_Book_Id
 * @property integer $Article_State
 * @property string $Published_Date
 * @property string $Expiry_Date
 * @property integer $Author_User_Id
 * @property string $Author_Unio_User_Id
 * @property string $Creation_Date
 * @property resource $Article_Thumbnail
 * @property string $Updated_On
 * @property boolean $Is_Deleted
 * @property string $Article_Description
 * @property integer $Magazine_Id
 */
class MagArticlesMaster extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mag_articles_master';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Unio_Book_Id', 'Article_State', 'Author_User_Id', 'Magazine_Id'], 'integer'],
            [['Published_Date', 'Expiry_Date', 'Creation_Date', 'Updated_On'], 'safe'],
            [['Article_Thumbnail'], 'string'],
            [['Is_Deleted'], 'boolean'],
            [['Magazine_Id'], 'required'],
            [['Article_Name'], 'string', 'max' => 255],
            [['Author_Unio_User_Id', 'Article_Description'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Article_Name' => 'Article  Name',
            'Unio_Book_Id' => 'Unio  Book  ID',
            'Article_State' => 'Article  State',
            'Published_Date' => 'Published  Date',
            'Expiry_Date' => 'Expiry  Date',
            'Author_User_Id' => 'Author  User  ID',
            'Author_Unio_User_Id' => 'Author  Unio  User  ID',
            'Creation_Date' => 'Creation  Date',
            'Article_Thumbnail' => 'Article  Thumbnail',
            'Updated_On' => 'Updated  On',
            'Is_Deleted' => 'Is  Deleted',
            'Article_Description' => 'Article  Description',
            'Magazine_Id' => 'Magazine  ID',
        ];
    }
}
