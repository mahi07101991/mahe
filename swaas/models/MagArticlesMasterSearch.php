<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MagArticlesMaster;

/**
 * MagArticlesMasterSearch represents the model behind the search form about `app\models\MagArticlesMaster`.
 */
class MagArticlesMasterSearch extends MagArticlesMaster
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'Unio_Book_Id', 'Article_State', 'Author_User_Id', 'Article_Description', 'Magazine_Id'], 'integer'],
            [['Article_Name', 'Published_Date', 'Expiry_Date', 'Author_Unio_User_Id', 'Creation_Date', 'Article_Thumbnail', 'Updated_On'], 'safe'],
            [['Is_Deleted'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MagArticlesMaster::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'Unio_Book_Id' => $this->Unio_Book_Id,
            'Article_State' => $this->Article_State,
            'Published_Date' => $this->Published_Date,
            'Expiry_Date' => $this->Expiry_Date,
            'Author_User_Id' => $this->Author_User_Id,
            'Creation_Date' => $this->Creation_Date,
            'Updated_On' => $this->Updated_On,
            'Is_Deleted' => $this->Is_Deleted,
            'Article_Description' => $this->Article_Description,
            'Magazine_Id' => $this->Magazine_Id,
        ]);

        $query->andFilterWhere(['like', 'Article_Name', $this->Article_Name])
            ->andFilterWhere(['like', 'Author_Unio_User_Id', $this->Author_Unio_User_Id])
            ->andFilterWhere(['like', 'Article_Thumbnail', $this->Article_Thumbnail]);

        return $dataProvider;
    }
}
