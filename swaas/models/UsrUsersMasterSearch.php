<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UsrUsersMaster;

/**
 * UsrUsersMasterSearch represents the model behind the search form about `app\models\UsrUsersMaster`.
 */
class UsrUsersMasterSearch extends UsrUsersMaster
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['User_Id', 'User_Type', 'Is_Deleted', 'Is_Optedout'], 'integer'],
            [['Union_User_Id', 'User_First_Name', 'User_Last_Name', 'User_Email', 'Update_On', 'password', 'Optedout_Date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchAdmin($params)
    {
        $query = UsrUsersMaster::find()->where('User_Type=3');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'User_Id' => $this->User_Id,
            'User_Type' => $this->User_Type,
            'Update_On' => $this->Update_On,
            'Is_Deleted' => $this->Is_Deleted,
            'Is_Optedout' => $this->Is_Optedout,
            'Optedout_Date' => $this->Optedout_Date,
        ]);

        $query->andFilterWhere(['like', 'Union_User_Id', $this->Union_User_Id])
            ->andFilterWhere(['like', 'User_First_Name', $this->User_First_Name])
            ->andFilterWhere(['like', 'User_Last_Name', $this->User_Last_Name])
            ->andFilterWhere(['like', 'User_Email', $this->User_Email])
            ->andFilterWhere(['like', 'password', $this->password]);

        return $dataProvider;
    }
    public function searchHcp($params)
    {
        $query = UsrUsersMaster::find()->where('User_Type=4 or User_Type=2');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'User_Id' => $this->User_Id,
            'User_Type' => $this->User_Type,
            'Update_On' => $this->Update_On,
            'Is_Deleted' => $this->Is_Deleted,
            'Is_Optedout' => $this->Is_Optedout,
            'Optedout_Date' => $this->Optedout_Date,
        ]);

        $query->andFilterWhere(['like', 'Union_User_Id', $this->Union_User_Id])
            ->andFilterWhere(['like', 'User_First_Name', $this->User_First_Name])
            ->andFilterWhere(['like', 'User_Last_Name', $this->User_Last_Name])
            ->andFilterWhere(['like', 'User_Email', $this->User_Email])
            ->andFilterWhere(['like', 'password', $this->password]);

        return $dataProvider;
    }
    public function searchRep($params)
    {
        $query = UsrUsersMaster::find()->where('User_Type=1');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'User_Id' => $this->User_Id,
            'User_Type' => $this->User_Type,
            'Update_On' => $this->Update_On,
            'Is_Deleted' => $this->Is_Deleted,
            'Is_Optedout' => $this->Is_Optedout,
            'Optedout_Date' => $this->Optedout_Date,
        ]);

        $query->andFilterWhere(['like', 'Union_User_Id', $this->Union_User_Id])
            ->andFilterWhere(['like', 'User_First_Name', $this->User_First_Name])
            ->andFilterWhere(['like', 'User_Last_Name', $this->User_Last_Name])
            ->andFilterWhere(['like', 'User_Email', $this->User_Email])
            ->andFilterWhere(['like', 'password', $this->password]);

        return $dataProvider;
    }
}
