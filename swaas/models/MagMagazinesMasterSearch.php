<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MagMagazinesMaster;

/**
 * MagMagazinesMasterSearch represents the model behind the search form about `app\models\MagMagazinesMaster`.
 */
class MagMagazinesMasterSearch extends MagMagazinesMaster
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'Magazine_Product', 'Author_User_Id'], 'integer'],
            [['Magazine_Name', 'Magazine_Thumbnail', 'Updated_On', 'Magazine_Description'], 'safe'],
            [['Is_Deleted'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MagMagazinesMaster::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'Magazine_Product' => $this->Magazine_Product,
            'Updated_On' => $this->Updated_On,
            'Is_Deleted' => $this->Is_Deleted,
            'Author_User_Id' => $this->Author_User_Id,
        ]);

        $query->andFilterWhere(['like', 'Magazine_Name', $this->Magazine_Name])
            ->andFilterWhere(['like', 'Magazine_Thumbnail', $this->Magazine_Thumbnail])
            ->andFilterWhere(['like', 'Magazine_Description', $this->Magazine_Description]);

        return $dataProvider;
    }
}
